# PhotoGetter - The reddit api photo downloader

With PhotoGetter, you can download a specified amount of .img and .png submissions from a reddit subreddit.

# Overview
PhotoGetter python reddit photo downloader is made to skip the hassle of individually downloading images. Skip opening up a browser and populate folders quickly with images from your favorite subreddit.

 - Download from multiple subreddits.
 - Save them in specified folders.
 - Coming soon: OC flag to only download original content. 


## Usage

In the following paragraphs, I am going to describe how you can get and use PhotoGetter for your own projects.

###  Getting it

To download photogetter, either fork this github repo or simply use Pypi via pip.
```sh
$ pip install photogetter
```

### Using it

## .env

Create a .env file in project directory with the structure:

```markdown
CLIENT_ID=
CLIENT_SECRET=
USER_AGENT=
USERNAME= *Optional*
PASSWORD= *Optional*
```
## Importing

After that you are ready to import
```python
from photogetter import PhotoGetter
```
and initialize to confirm login through .env file.

```python
pg = PhotoGetter()

# Note you can pass in an array of 6 booleans 
    # to represent the 6 tabs in each subreddit
    # 'Hot', 'Top', 'New', 'Rising', 'Controversial', 'Gilded'
# pg = PhotoGetter(tabboolarray=[1, 1, 0, 0, 0, 0])
```

## Downloading

After it is imported you can download photos  
- Amount = Amount of photos to download from each tab
- Subredditname = Name of subreddit to fetch from
```python
pg.getPhotos(amount=10, subredditname='pics')
```

## Save
And finally save them all to a folder

```python
pg.savePhotos(savefolder='photos')
```

## Finalize

Check if the photos have been saved inside of specified save folder.

License
----

MIT License

Copyright (c) 2021 Christopher Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.