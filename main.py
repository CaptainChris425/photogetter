from photogetter import PhotoGetter
import os


def main():
    pg = PhotoGetter(tabboolarray=[1, 1, 0, 0, 0, 0])
    pg.getPhotos(5, 'earthporn')
    pg.getPhotos(5, 'pics')
    pg.savePhotos('photos')


if __name__ == '__main__':
    main()
