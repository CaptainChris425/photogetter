from distutils.core import setup

setup(
    name='photogetter',
    packages=['photogetter'],
    version='0.1',
    license='MIT',
    description='Downloads images from specified subreddit',
    author='Christopher Young',
    author_email='young.christopher.425@gmail.com',
    url='https://gitlab.com/CaptainChris425/photogetter',
    download_url='https://gitlab.com/CaptainChris425/photogetter/-/releases/v1.0',
    keywords=['REDDIT', 'PHOTOS', 'DOWNLOADER'],
    install_requires=[
        'praw',
        'requests'
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.8'
    ],
)
