import prawcore.exceptions
import requests
import logging
import praw
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())
osGet = os.environ.get

logging.basicConfig()
logger = logging.getLogger(__name__)


# logger.setLevel(logging.DEBUG)


def createRedditReadInstance(client_id='', client_secret='',
                             user_agent=''):
    return praw.Reddit(client_id=client_id,
                       client_secret=client_secret,
                       user_agent=user_agent)


def createRedditWriteInstance(client_id='', client_secret='',
                              user_agent='', username='',
                              password=''):
    return praw.Reddit(client_id=client_id,
                       client_secret=client_secret,
                       user_agent=user_agent,
                       username=username,
                       password=password)


def getSubredditInstance(client_id, client_secret, user_agent, username, password, read=True):
    if read:
        return createRedditReadInstance(client_id,
                                        client_secret,
                                        user_agent)
    else:
        return createRedditWriteInstance(client_id,
                                         client_secret,
                                         user_agent,
                                         username,
                                         password)


def buildSaveName(folder, title, ext):
    """Returns a formatted title to function as the file name when saved"""
    title = title.replace('.', '').replace(':', '').replace('\'', '').replace('\"', '').replace(' ', '_')
    return folder + '\\' + str(title) + ext


def getLogin():
    """Gets Reddit API login credentials.

    :returns
        (client_id, client_secret, user_agent, username, password) from .env
    :except
        ValueError: Incorrect or Missing .env file
    """
    client_id = osGet('CLIENT_ID')
    client_secret = osGet('CLIENT_SECRET')
    user_agent = osGet('USER_AGENT')
    username = osGet('USERNAME')
    password = osGet('PASSWORD')
    ret = (client_id, client_secret, user_agent, username, password)
    if None in ret[0:3]:
        logger.error('Incorrect or missing .env file, please read README.md')
        raise ValueError('Incorrect or missing .env file, please read README.md')
    return ret


class PhotoGetter(object):
    """
    A class to get photos from reddit api

    ...
    Attributes
    ----------
    instanceCreator: instancecreator::InstanceCreator
        creates reddit instances

    Methods
    -------
    info(additional=""):
        Prints the person's name and age.
    """

    def __init__(self, tabboolarray=None):
        self.tabBoolArray = [False] * 6  # [Hot, Top, New, Rising, Controversial, Gilded]
        if tabboolarray:
            self.setTabArray(tabboolarray)

        self.login = \
            (self.client_id, self.client_secret, self.user_agent, self.username, self.password) = \
            getLogin()

        self.subRedditInstance = getSubredditInstance(*self.login)  # praw instance of subreddit
        self.photos = []

    def setTabArray(self, tabarray: list = None):
        # If the length is correct
        # set values to true if there is a value there
        # this way users can enter many different true values
        if tabarray and len(tabarray) == 6:
            for i, v in enumerate(tabarray):
                if v:
                    self.tabBoolArray[i] = True
        else:
            self.getTabArrayFromUser()

    def getTabArrayFromUser(self):
        # Ask user from input for each subreddit section
        print('Enter 1/0 for each tab to include or not:')
        options = ['Hot: ', 'Top: ', 'New: ', 'Rising: ', 'Controversial: ', 'Gilded: ']
        for i, v in enumerate(options):
            answer = int(input(v))
            if answer:
                self.tabBoolArray[i] = True

    def getPhotos(self, amount=10, subredditname=None):
        logger.info(f'get photos {self.tabBoolArray}')
        try:
            subReddit = self.subRedditInstance.subreddit(subredditname)
            tabArray = [subReddit.hot, subReddit.top, subReddit.new, subReddit.rising, subReddit.controversial,
                        subReddit.gilded]
            for i, b in enumerate(self.tabBoolArray):
                if b:
                    for submission in tabArray[i](limit=amount):
                        self.photos.append(submission)
            logger.info(f'photos gotten {self.photos}')
        except prawcore.exceptions.Redirect:
            logger.error(f'Subreddit name, {subredditname}, does not exist')
            return
        except prawcore.exceptions.RequestException as e:
            logger.error(f'prawcore.exceptions.RequestException: {e} - Check .env')
            return

    def savePhotos(self, savefolder, oc=False):
        if not os.path.isdir(savefolder):
            os.mkdir(savefolder)
        skips, saves = 0, 0
        for submission in self.photos:
            title = submission.title
            url = submission.url
            try:
                logger.info(f'getting post id: {submission.id}, author: {submission.author.name}')
                if 'jpg' in url.split(sep='.') or 'png' in url.split(sep='.'):
                    ext = '.jpg' if 'jpg' in url.split(sep='.') else '.png'
                    saveName = buildSaveName(savefolder, title, ext)
                    imgContent = requests.get(url).content
                    try:
                        with open(saveName, 'wb') as file:
                            file.write(imgContent)
                            logger.info(f'Saved photo ID {submission.id} as {saveName}')
                            saves += 1
                    except OSError:
                        logger.info(f'Skipping post ID: {submission.id}')
                        skips += 1
                else:
                    logger.info(url.split(sep='.'))
            except AttributeError:
                logger.info(f'Skipping post ID: {submission.id}: No author')
                skips += 1
        logger.info(f'Saved {saves} : Skipped {skips} : Total {saves + skips}')
